defmodule InnCheckerWeb.HistoryController do
  use InnCheckerWeb, :controller
  alias InnChecker.Checks

  def index(conn, _params) do
    render(conn, "index.html", checks_history: Checks.list_checks_history(), input: "", error: "")
  end

  def check(conn, %{"inn" => inn}) do
    case Checks.create_check(inn) do
      {:ok, _new_check} ->
        render(conn, "index.html", checks_history: Checks.list_checks_history(), input: "", error: "")
      {:error, error} ->
        render(conn, "index.html", checks_history: Checks.list_checks_history(), input: inn, error: error)
    end
  end
end
