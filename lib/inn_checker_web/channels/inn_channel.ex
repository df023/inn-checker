defmodule InnCheckerWeb.InnChannel do
  use InnCheckerWeb, :channel
  alias InnChecker.Checks

  def join("inn:lobby", _payload, socket) do
      {:ok, socket}
  end

  def handle_in("check", %{"inn" => inn}, socket) do
    case Checks.create_check(inn) do
      {:ok, new_check} ->
        {:reply, {:ok, %{new_check | inserted_at: Checks.format_date(new_check.inserted_at)}}, socket}
      {:error, error} ->
        {:reply, {:error, %{inn: error}}, socket}
    end
  end
end
