defmodule InnCheckerWeb.Router do
  use InnCheckerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  scope "/", InnCheckerWeb do
    pipe_through :browser
    
    get "/", HistoryController, :index
    post "/", HistoryController, :check
  end
end
