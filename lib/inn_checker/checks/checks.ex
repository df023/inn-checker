defmodule InnChecker.Checks do
  @moduledoc """
  The Checks context.
  """
  use Timex
  import Ecto.Query, warn: false
  alias InnChecker.Repo
  alias InnChecker.Checks.History

  @doc """
  Returns the list of checks_history.
  """
  def list_checks_history do
    Repo.all(from h in History, order_by: [desc: :inserted_at])
    |> Enum.map(fn check -> 
      %{check | inserted_at: format_date(check.inserted_at)} end)
  end

  def format_date(date) do
    {:ok, date} = 
      Timezone.convert(date, "Europe/Moscow")
      |> Timex.format("[{D}.{0M}.{YYYY} {h24}:{m}]")
    date
  end

  @doc """
  Creates a new inn check.
  """
  def create_check(inn) do
    case validate_inn(inn) do
      {:ok, inn, length} ->
        inn_list = String.to_integer(inn)
        |> Integer.digits()
        
        valid = if length == 10 do
          is_valid_inn_sequence?(Enum.slice(inn_list, 0..8), List.last(inn_list))
        else
          [check_sum1, check_sum2] = Enum.slice(inn_list, -2..-1)     
          is_valid_inn_sequence?(Enum.slice(inn_list, 0..9), check_sum1) && 
          is_valid_inn_sequence?(Enum.slice(inn_list, 0..10), check_sum2)
        end
          
        History.changeset(%History{}, %{"inn" => inn, "valid" => valid}) |> Repo.insert

      {:error, reason} -> {:error, reason}
    end
  end
  
  @doc """
  Provides input validation

  ## Examples

    iex> InnChecker.Checks.validate_inn("")
    {:error, "Нужно заполнить"}
    iex> InnChecker.Checks.validate_inn("908234")
    {:error, "Должен быть длинной в 10 или 12 цифр"}

  """
  def validate_inn(inn) do
    inn = String.trim(inn)
    length = String.length(inn)
    cond do
      inn == "" ->
        {:error, "Нужно заполнить"}

      !Regex.match?(~r{^\d+$}, inn) ->
        {:error, "Должен состоять только из цифр"}
    
      length != 10 && length != 12 ->
        {:error, "Должен быть длинной в 10 или 12 цифр"}
      
      Repo.one(from h in History, where: h.inn == ^inn) ->
        {:error, "Уже проверяли"}

      true -> {:ok, inn, length}
    end
  end

  @doc """
  Determines wheter inn sequence is correct

  ## Examples

    iex> InnChecker.Checks.is_valid_inn_sequence?([6,3,1,6,1,4,5,6,7], 7)
    true

    # iex> InnChecker.Checks.is_valid_inn_sequence?([5,0,2,1,0], 5)
    false

  """
  def is_valid_inn_sequence?(seq, check_sum) do
    base = [2, 4, 10, 3, 5, 9, 4, 6, 8]
    mults = case length(seq) do
      9 -> base
      10 -> [7] ++ base
      11 -> [3, 7] ++ base
    end

    rem = Enum.zip(seq, mults)
    |> Enum.map(fn {item, mult} -> item * mult end)
    |> Enum.sum()
    |> rem(11)
    |> rem(10)

    rem == check_sum
  end
end
