defmodule InnChecker.Checks.History do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Jason.Encoder, only: [:inn, :valid, :inserted_at]}

  schema "checks_history" do
    field :inn, :string
    field :valid, :boolean, default: false

    timestamps()
  end

  @doc false
  def changeset(history, attrs) do
    history
    |> cast(attrs, [:inn, :valid])
    |> validate_required([:inn], message: "Нужно заполнить")
    |> validate_format(:inn, ~r{^\d+$}, message: "Должен состоять только из цифр")
    |> validate_format(:inn, ~r/^(\d{10}|\d{12})$/, message: "Должен быть длинной в 10 или 12 цифр")
  end
end
