import { Socket } from "phoenix";
import "../css/app.css";

const socket = new Socket("/socket", {});
socket.connect();

const checkList = document.querySelector(".inn-check__list");
const innInput = document.querySelector("#inn-validate");
const errorElem = document.querySelector(".inn-check__error");

const channel = socket.channel("inn:lobby", {});
channel.join().receive("error", resp => {
  console.log("Unable to join", resp);
});

document.querySelector(".inn-check__form").addEventListener("submit", e => {
  e.preventDefault();
  const inn = innInput.value;
  channel
    .push("check", { inn })
    .receive("ok", resp => {
      const { inn, inserted_at, valid } = resp;

      const content = `
      <span class="inn-check__date">${inserted_at}</span>
      <div class="inn-group">
        <span class="inn-check__number">&nbsp;${inn} : </span>
        ${
          valid
            ? '<span class="inn-valid">корректен</span>'
            : '<span class="inn-invalid">некорректен</span>'
        }          
      </div>
      `;
      const newCheck = document.createElement("li");
      newCheck.classList.add("inn-check__list-item");
      newCheck.innerHTML = content;
      checkList.prepend(newCheck);
      innInput.value = "";
      errorElem.textContent = "";
    })
    .receive("error", resp => {
      errorElem.textContent = resp.inn;
    });
});
