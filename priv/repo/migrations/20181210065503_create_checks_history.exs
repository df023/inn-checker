defmodule InnChecker.Repo.Migrations.CreateChecksHistory do
  use Ecto.Migration

  def change do
    create table(:checks_history) do
      add :inn, :string
      add :valid, :boolean, default: false, null: false
      timestamps()
    end
  end
end
