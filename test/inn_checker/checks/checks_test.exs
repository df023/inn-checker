defmodule InnChecker.ChecksTest do
  use InnChecker.DataCase
  doctest InnChecker.Checks

  alias InnChecker.Checks

  @moduletag :test_checks

  describe "checks_history" do
    alias InnChecker.Checks.History

    @valid_inn "6316145677"
    @invalid_inn "6316145"
    @valid_inns ["7617008852", "5261073080", "7733807165"]
    @invalid_inns ["1840008612", "7716646134", "6025032590"]

    test "list_checks_history/0 returns all checks_history" do
      checks = 
        @valid_inns ++ @invalid_inns
        |> Enum.map(fn inn ->
            {:ok, check} = Checks.create_check(inn)
            check
          end)
        |> Enum.map(fn check -> 
            %{check | inserted_at: Checks.format_date(check.inserted_at)} 
          end)
      
      
      assert Checks.list_checks_history() == checks
    end

    test "create_check/1 with valid data creates a check_history" do
      assert {:ok, %History{} = history} = Checks.create_check(@valid_inn)
      assert history.inn == "6316145677"
      assert history.valid == true
    end

    test "create_check/1 with invalid data returns an error" do
      assert {:error, "Должен быть длинной в 10 или 12 цифр"} = Checks.create_check(@invalid_inn)
    end
  end

  describe "inn validation" do
    setup do
      Checks.create_check("123123123123")
      :ok
    end

    test "validate_inn/1 returns a proper error for empty value" do
     assert {:error, "Нужно заполнить"} == Checks.validate_inn("")
    end

    test "validate_inn/1 returns a proper error for non numeric input" do
      assert {:error, "Должен состоять только из цифр"} == Checks.validate_inn("123asdf")
    end
    
    test "validate_inn/1 returns a proper error for input with invalid length" do
      assert {:error, "Должен быть длинной в 10 или 12 цифр"} == Checks.validate_inn("123123123")
    end

    test "validate_inn/1 returns a proper error if check for given input already exists" do
      assert {:error, "Уже проверяли"} == Checks.validate_inn("123123123123")
    end
  end

  test "is_valid_inn_sequence?/1 returns true for valid input" do
    assert Checks.is_valid_inn_sequence?([6,3,1,6,1,4,5,6,7], 7)
  end

  test "is_valid_inn_sequence?/1 returns false for invalid input" do
    refute Checks.is_valid_inn_sequence?([6,3,1,6,1,4,5,6,7], 3)
  end

end
