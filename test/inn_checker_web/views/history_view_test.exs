defmodule InnCheckerWeb.HistoryViewTest do
  use InnCheckerWeb.ConnCase, async: true
  import Phoenix.View

  test "renders index.html" do
    resp = render_to_string(InnCheckerWeb.HistoryView, "index.html", 
      conn: build_conn(), checks_history: nil, input: nil, error: nil
    )

    assert resp =~ "для юридических и физических лиц"
  end
end
