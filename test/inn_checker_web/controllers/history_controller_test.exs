defmodule InnCheckerWeb.HistoryControllerTest do
  use InnCheckerWeb.ConnCase

  alias InnChecker.Checks

  @create_attrs "123123123123"
  @invalid_attrs "123123asdf"

  def fixture(:history) do
    {:ok, history} = Checks.create_history(@create_attrs)
    history
  end

  describe "index" do
    test "lists all checks_history", %{conn: conn} do
      conn = get(conn, Routes.history_path(conn, :index))
      assert html_response(conn, 200) =~ "Введите ИНН"
    end
  end
  
  describe "check" do
    test "adds a new check to the list", %{conn: conn} do
      conn = post(conn, Routes.history_path(conn, :check), inn: @create_attrs)
      assert html_response(conn, 200) =~ "123123123123"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.history_path(conn, :check), inn: @invalid_attrs)
      assert html_response(conn, 200) =~ "Должен состоять только из цифр"
    end
  end
end
