defmodule InnCheckerWeb.InnChannelTest do
  use InnCheckerWeb.ChannelCase

  setup do
    {:ok, _, socket} =
      socket(InnCheckerWeb.InnSocket, "user_id", %{some: :assign})
      |> subscribe_and_join(InnCheckerWeb.InnChannel, "inn:lobby")
    {:ok, socket: socket}
  end

  test "check replies with status ok", %{socket: socket} do
    ref = push socket, "check", %{"inn" => "123123123123"}
    assert_reply ref, :ok, %{inn: "123123123123", valid: false}
  end
end
